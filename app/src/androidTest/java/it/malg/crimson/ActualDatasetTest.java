package it.malg.crimson;

import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import it.malg.crimson.model.Additive;
import it.malg.crimson.model.Database;
import it.malg.crimson.model.Dataset;
import it.malg.crimson.model.Request;
import it.malg.crimson.model.Response;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;


/**
 * Dataset population test.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ActualDatasetTest {
    public ActualDatasetTest() {
        Database.initInstance(InstrumentationRegistry.getTargetContext());
    }
    /**
     * Test #1: Pull dataset from remote.
     */
    @Test
    public void t01_dataset() throws Exception {
        Dataset di = Dataset.getInstance();
        //assertEquals("Dataset mustn't be ready yet.", false, di.isReady());
        assertEquals("Default remote-URL must be preset", "https://circabc.europa.eu/d/a/workspace/SpacesStore/2387736c-833c-49be-8f08-f72e297bf3fd/FoodAdditives.zip", di.getURL());
        assertEquals("Call to pull(true) must return true", true, di.pull(true)); //di.pull(true)
        assertEquals("Dataset must be ready now.", true, di.isReady());
        assertNotEquals("Local vesion must be > 0", 0, di.getLocalVersion());
        assertEquals("Call to pull() *now* should return false", false, di.pull()); //di.pull(false)
    }
    @Test
    public void t10_additive() {
        Additive add = Additive.fromName("carotenes");
        assertEquals("Name verification", "Carotenes", add.getName());
        assertEquals("E-Number verification", "160a", add.getENumber());
        assertEquals("Display name verification", "Carotenes (E160a)", add.getDisplayName());
        assertEquals("Approval status verification", Additive.eApprovalStatus.Approved, add.getApprovalStatus());
        assertNotEquals("Approval timestamp must be set", 0, add.getApprovalTimestamp());
        assertNotEquals("Document URL must be provided", "" , add.getDocument());
    }
    @Test
    public void t20_request() {
        Request req = new Request(new String[]{"carotenes", "dihydrogen Monoxide"});
        long ts = req.getTimestamp();
        assertNotEquals("Timestamp must be > 0", 0, ts);
        assertEquals("Queries count verification", 2, req.getQuery().length);
        req.retryAll();
        assertEquals("Timestamp mustn't change after retryAll()", ts, req.getTimestamp());
    }
    @Test
    public void t30_responses() {
        Request req = new Request(new String[]{"carotenes", "dihydrogen Monoxide"});
        assertEquals("Responses count verification", 2, req.getReponses().length);
        // Response[0]
        Response rsp0 = req.getReponses()[0];
        assertEquals("rsp[0].getQuery() validation", "carotenes", rsp0.getOriginalQuery());
        assertEquals("rsp[0] must be successful", Response.eStatus.Successful, rsp0.getResponseStatus());
        assertEquals("rsp[0].getApprovalStatus() must be Positive", Additive.eApprovalStatus.Approved, rsp0.getApprovalStatus());
        assertEquals("rsp[0].getOriginalApprovalStatus() must be Positive", Additive.eApprovalStatus.Approved, rsp0.getOriginalApprovalStatus());
        assertEquals("rsp[0].getAdditive() must match 'Carotenes' additive", "Carotenes", rsp0.getAdditive().getName());
        assertNotEquals("rsp[0].getTimestamp() must be > 0", 0, rsp0.getTimestamp());
        assertEquals("rsp[0].getTimestamp() must be = rsp[0].getOriginalTimestamp()", rsp0.getOriginalTimestamp(), rsp0.getTimestamp());
        rsp0.retry();
        assertNotEquals("rsp[0].getTimestamp() must be != rsp[0].getOriginalTimestamp() after retry()", rsp0.getOriginalTimestamp(), rsp0.getTimestamp());
        // Response[1]
        Response rsp1 = req.getReponses()[1];
        assertEquals("rsp[1].getQuery() validation", "dihydrogen Monoxide", rsp1.getOriginalQuery());
        assertEquals("rsp[1] must be unsuccessful (not found)", Response.eStatus.NotFound, rsp1.getResponseStatus());
        assertNull("rsp[1].getAdditive() must be null", rsp1.getAdditive());
        assertNotEquals("rsp[1].getTimestamp() must be > 0", 0, rsp1.getTimestamp());
        assertEquals("rsp[1].getTimestamp() must be = rsp[1].getOriginalTimestamp()", rsp1.getOriginalTimestamp(), rsp1.getTimestamp());
        rsp1.retry();
        assertNotEquals("rsp[1].getTimestamp() must be != rsp[1].getOriginalTimestamp() after retry()", rsp1.getOriginalTimestamp(), rsp1.getTimestamp());
    }
}
