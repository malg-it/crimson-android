package it.malg.crimson;

public class Sostanza {
    String nome;
    String responso;
    String dataAcquisizione;
    String dataControllo;
    String categoria;
    String subCategoria;
    String dataDataset;
    String document;
    int colore;

    public Sostanza(String nome, String responso, String dataAcquisizione, String dataControllo, String categoria, String subCategoria, String dataDataset,String document, int colore) {
        this.nome = nome;
        this.responso = responso;
        this.dataAcquisizione = dataAcquisizione;
        this.dataControllo = dataControllo;
        this.categoria = categoria;
        this.subCategoria = subCategoria;
        this.dataDataset = dataDataset;
        this.document=document;
        this.colore = colore;
    }

    public String getNome() { return nome; }
    public void setNome(String nome) { this.nome = nome; }

    public String getResponso() { return responso; }
    public void setResponso(String responso) { this.responso = responso; }

    public String getDataAcquisizione() { return dataAcquisizione; }
    public void setDataAcquisizione(String dataAcquisizione) { this.dataAcquisizione = dataAcquisizione; }

    public String getDataControllo() { return dataControllo; }
    public void setDataControllo(String dataControllo) { this.dataControllo = dataControllo; }

    public String getCategoria() { return categoria; }
    public void setCategoria(String categoria) { this.categoria = categoria; }

    public String getSubCategoria() { return subCategoria;}
    public void setSubCategoria(String subCategoria) { this.subCategoria = subCategoria; }

    public String getDataDataset() { return dataDataset; }
    public void setDataDataset(String dataDataset) { this.dataDataset = dataDataset; }

    public String getDocument() { return document; }
    public void setDocument(String document) { this.document = document; }

    public int getColore() { return colore; }
    public void setColore(int colore) { this.colore = colore; }

}
