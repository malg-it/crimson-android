package it.malg.crimson;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

import it.malg.crimson.model.Database;
import it.malg.crimson.model.Dataset;

public class SplashScreenActivity extends Activity {

    TextView versionTv;
    private static int SPLASH_TIME_OUT = 900;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);

        String version = BuildConfig.VERSION_NAME; //BuildConfig.VERSION_CODE;
        versionTv = (TextView) findViewById(R.id.version_number);
        versionTv.setText("Version: "+version);

        Database.initInstance(this);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                boolean datasetDownloaded = Dataset.getInstance().isReady();

                if(datasetDownloaded){
                Intent i = new Intent(SplashScreenActivity.this, MainActivity.class);
                startActivity(i);
            }else {
                Intent i = new Intent(SplashScreenActivity.this, UpdateActivity.class);
                startActivity(i);
            }
            finish();
            }
        }, SPLASH_TIME_OUT);
    }

}

