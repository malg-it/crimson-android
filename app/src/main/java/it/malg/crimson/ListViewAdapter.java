package it.malg.crimson;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class ListViewAdapter extends BaseAdapter {

    Context context;
    LayoutInflater inflater;
    private List<Sostanza> subastanceList = null;
    private ArrayList<Sostanza> arraylist;

    public ListViewAdapter(Context context, List<Sostanza> subastanceList) {
        this.context = context;
        this.subastanceList = subastanceList;
        inflater = LayoutInflater.from(this.context);
        this.arraylist = new ArrayList<Sostanza>();
        this.arraylist.addAll(subastanceList);
    }

    public class ViewHolder {
        TextView nomeText;
        TextView responsoText;
        TextView dataControlloText;
        ImageView pallinoImage;
    }

    @Override
    public int getCount() {
        return subastanceList.size();
    }

    @Override
    public Sostanza getItem(int position) {
        return subastanceList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.listview_item, null);
            holder.nomeText = (TextView) view.findViewById(R.id.nome_sostanza);
            holder.responsoText = (TextView) view.findViewById(R.id.responso);
            holder.dataControlloText = (TextView) view.findViewById(R.id.ultimo_controllo);
            holder.pallinoImage = (ImageView) view.findViewById(R.id.simbolo_responso);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.nomeText.setText(subastanceList.get(position).getNome());// Set the results
        holder.responsoText.setText(subastanceList.get(position).getResponso());
        holder.dataControlloText.setText(subastanceList.get(position).getDataControllo());
        holder.pallinoImage.setColorFilter(subastanceList.get(position).getColore());

        return view;
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        subastanceList.clear();
        if (charText.length() == 0) {
            subastanceList.addAll(arraylist);
        } else {
            for (Sostanza wp : arraylist) {
                if (wp.getNome().toLowerCase(Locale.getDefault()).contains(charText)) {
                    subastanceList.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }

}