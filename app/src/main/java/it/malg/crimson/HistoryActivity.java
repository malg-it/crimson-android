package it.malg.crimson;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewStub;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import it.malg.crimson.model.Response;

public class HistoryActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private RecyclerView recyclerView;
    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    int k=0;
    ArrayList<Response> r;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        r = Response.getLatest(0, 10);
        if (r.size() < 1) {
            ViewStub stub = (ViewStub) findViewById(R.id.viewStub);
            stub.setLayoutResource(R.layout.history_empty);
            View inflated = stub.inflate();
        } else {
            recyclerView = (RecyclerView) findViewById(R.id.recyclerView_small_card);
            recyclerView.setHasFixedSize(true);
            final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(layoutManager);

            getHistoryCard(0,10);

            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    if (dy > 0) {
                        visibleItemCount = layoutManager.getChildCount();
                        totalItemCount = layoutManager.getItemCount();
                        pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();

                        if (loading) {
                            if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                                //loading = false;
                                getHistoryCard(k,10);
                                k+=10;
                            }
                        }
                    }
                }
            });
        }
    }

    void getHistoryCard(int offset, int count){

        r = Response.getLatest(offset, count);
        if (r.size() > 1) {
            r = Response.getLatest(offset, count);
            int color = getResources().getColor(R.color.colorAccepted);

            List<Sostanza> mainData = new ArrayList<>();
            for (int i = 0; i < r.size(); i++) {
                if (r.get(i).getAdditive().getApprovalStatus().toString() == "Unknown") {
                    color = getResources().getColor(R.color.colorUnknown);
                } else if (r.get(i).getAdditive().getApprovalStatus().toString() == "Approved") {
                    getResources().getColor(R.color.colorAccepted);
                } else if (r.get(i).getAdditive().getApprovalStatus().toString() == "Rejected") {
                    color = getResources().getColor(R.color.colorReject);
                }

                Date dateOriginal = new Date(r.get(i).getOriginalTimestamp());
                SimpleDateFormat df2 = new SimpleDateFormat("dd/MM/yy");
                String originalTimeStr = df2.format(dateOriginal);

                Date dateLastCheck = new Date(r.get(i).getTimestamp());
                String lastTimeStr = df2.format(dateLastCheck);

                mainData.add(new Sostanza(r.get(i).getAdditive().getDisplayName(), r.get(i).getApprovalStatus().toString(), originalTimeStr, lastTimeStr, "", "", "", "", color));
            }
            RecyclerView.Adapter adapter = new AdapterSmall(mainData);
            recyclerView.setAdapter(adapter);
        }
    }


    @Override
    public void onBackPressed () {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

        /*
        @Override
        public boolean onCreateOptionsMenu (Menu menu){
            // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(R.menu.drawer, menu);
            return true;
        }

        @Override
        public boolean onOptionsItemSelected (MenuItem item){
            int id = item.getItemId();
            Context context = this;
            if (id == R.id.action_settings) {
                return true;
            }

            return super.onOptionsItemSelected(item);
        }
        */

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected (MenuItem item){
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Context context = this;
        if (id == R.id.nav_analisi) {
            Intent intent = new Intent(context, MainActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_cronologia) {

        } else if (id == R.id.nav_indice) {
            Intent intent = new Intent(context, IndexActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_manuale) {
            Intent intent = new Intent(context, GuideActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_aggiornamenti) {
            Intent intent = new Intent(context, UpdateActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_share) {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, R.string.share_message);
            sendIntent.setType("text/plain");
            startActivity(sendIntent);
        } else if (id == R.id.nav_send) {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{getString(R.string.mail_recipient)}); // !
            sendIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.mail_subject));
            sendIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.email_body));
            sendIntent.setType("text/plain");
            startActivity(sendIntent);
        } else if (id == R.id.nav_credits) {
            Intent intent = new Intent(context,CreditsActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
