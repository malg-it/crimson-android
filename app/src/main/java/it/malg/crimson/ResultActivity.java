package it.malg.crimson;

import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewStub;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import it.malg.crimson.model.Additive;
import it.malg.crimson.model.Database;
import it.malg.crimson.model.Request;
import it.malg.crimson.model.Response;


public class ResultActivity extends AppCompatActivity {

    String query;
    Pattern TAG_REGEX = Pattern.compile("<(.+?)>");


    List<String> getValues(String str) {
        List<String> tagValues = new ArrayList<String>();
        Matcher matcher = TAG_REGEX.matcher(str);
        while (matcher.find()) {
            tagValues.add(matcher.group(1));
        }
        return tagValues;
    }

    String formatQuery(String str) {
        String result ="";
        str = str.replaceAll("\\s\\s+"," ");

        if (str.contains(",")) {
            result = "<"+str.replaceAll(",","><")+">";
        }else if (str.matches("(.*)E(\\s)[0-9](.*)")) {

            Pattern pat = Pattern.compile("E\\s\\d*");
            Matcher mat = pat.matcher(str);
            String tmp="";

            while (mat.find())
                tmp+="<"+mat.group()+">";
            str = str.replaceAll("E\\s\\d*","");
            str = "<"+str.replaceAll(" ","><")+">";
            result = str+tmp;
        }else{
            result = "<"+str.replaceAll(" ","><")+">";
        }
        return result;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.noItemView);
        relativeLayout.setVisibility(View.GONE);

        handleIntent(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {

            RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView_small_card);
            recyclerView.setHasFixedSize(true);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(layoutManager);

            String query = intent.getStringExtra(SearchManager.QUERY);
            String strFormat = formatQuery(query);
            String[] test = new String[getValues(strFormat).size()];
            test = getValues(strFormat).toArray(test);

            List<Sostanza> mainData = new ArrayList<>();
            List<String> testVerifiedList = new ArrayList<String>();

            String x[] =test;

            for(int j=0;j<x.length;j++){
                Additive additives[] = Additive.search(x[j].replaceFirst("^\\s*",""),4);
                int p=0;
                while(p<additives.length){
                    testVerifiedList.add(0,additives[p].getName());
                    p++;
                }
            }

            String[] testVerifiedArray = new String[testVerifiedList.size()];
            testVerifiedArray = testVerifiedList.toArray(testVerifiedArray);
            boolean insert=false;
            Request request = new Request(testVerifiedArray);
            for(int jj=0;jj<testVerifiedArray.length;jj++) {
                if (Database.getInstance().getAdditiveBySubquery(testVerifiedArray[jj]).getCount() > 0 && testVerifiedArray[jj]!=null) {
                    insert=true;
                    Response reponses[] = request.getReponses();

                    int color= getResources().getColor(R.color.colorAccepted);
                    if (reponses[jj].getAdditive().getApprovalStatus().toString()=="Unknown"){
                        color= getResources().getColor(R.color.colorUnknown);
                    }else if (reponses[jj].getAdditive().getApprovalStatus().toString()=="Approved"){
                        getResources().getColor(R.color.colorAccepted);
                    }else if (reponses[jj].getAdditive().getApprovalStatus().toString()=="Rejected"){
                        color= getResources().getColor(R.color.colorReject);
                    }

                    Date dateOriginal=new Date(reponses[jj].getOriginalTimestamp());
                    SimpleDateFormat df2 = new SimpleDateFormat("dd/MM/yy");
                    String originalTimeStr = df2.format(dateOriginal);

                    Date dateLastCheck =new Date(reponses[jj].getTimestamp());
                    String lastTimeStr = df2.format(dateLastCheck);

                    mainData.add(new Sostanza(reponses[jj].getAdditive().getDisplayName(), reponses[jj].getApprovalStatus().toString(), originalTimeStr,lastTimeStr ,"","","","", color));
                }
            }
            if(!insert){
                RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.noItemView);
                relativeLayout.setVisibility(View.VISIBLE);
            }

            RecyclerView.Adapter adapter = new AdapterSmall(mainData);
            recyclerView.setAdapter(adapter);
        }
    }
}
