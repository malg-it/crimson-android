package it.malg.crimson.model.exceptions;

/**
 * Signals that a dataset pull operation has encountered a critical failure.
 */
public class DatasetUpdateException extends Exception {
    public DatasetUpdateException(String message) {
        super(message);
    }
    public DatasetUpdateException(String message, Exception cause) {
        super(message, cause);
    }
}
