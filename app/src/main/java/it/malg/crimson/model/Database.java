package it.malg.crimson.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * In-app SQLite access.
 */
public final class Database extends SQLiteOpenHelper {
    private Database(Context context) {
        super(context, "crimsonDB", null, 1);
    }
    private static Database instance;
    /**
     * Initialize the Database class. Must be called on application startup.
     * @param context Application context.
     */
    public static void initInstance(Context context) {
        Database.instance = new Database(context);
    }
    /**
     * Get the main Database instance.
     */
    public static Database getInstance() {
        return Database.instance;
    }
    /**
     * Called whenever the database needs upgrading.
     * @future Empty for now.
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        // Empty for now. Required in later versions.
    }

    /**
     * Called if the database hasn't been initialized yet.
     * @param db Database instance.
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.beginTransaction();
        try {
            // User-prefs
            db.execSQL("CREATE TABLE userprefs ("
                    + "id TEXT PRIMARY KEY,"
                    + "value TEXT NOT NULL)"
            );
            ContentValues cols = new ContentValues();
            cols.put("id", "dataset_version");
            cols.put("value", "0");
            db.insertOrThrow("userprefs", null, cols);

            // Additives
            db.execSQL("CREATE TABLE additives ("
                            + "id TEXT PRIMARY KEY,"
                            + "additiveName TEXT NOT NULL,"
                            + "eNumber TEXT,"
                            + "approvation INT DEFAULT 0,"
                            + "approvationDate INT)"
                    // + "referenceDoc TEXT )"
            );

            // Requests
            db.execSQL("CREATE TABLE requests ("
                    + "id INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + "initDate DATETIME NOT NULL )"
            );

            // Responses
            db.execSQL("CREATE TABLE responses ("
                            + "id INT PRIMARY KEY,"
                            + "subquery TEXT,"
                            + "req INT,"
                            + "status INT NOT NULL,"
                            + "additive TEXT,"
                            + "origTimestamp DATETIME,"
                            + "origResult INT,"
                            + "lastTimestamp DATETIME,"
                            + "lastResult INT)"
                    // SQLite does NOT support FOREIGN KEY(s)
                /*
                    + "FOREIGN KEY (req) REFERENCES request (id),"
                    + "FOREIGN KEY (additive) REFERENCES additives (id) )"
                */
            );
            Log.i("Database", "Successfully initialized!");
        } catch (Exception ex) {
            Log.e("Database.onCreate", "Initialization failure.", ex);
            throw ex;
        } finally {
            db.setTransactionSuccessful();
            db.endTransaction();
        }
    }

    //TODO: Inserire tutte le query SQL in metodi qui sotto:
//<editor-fold desc="Dataset">
    private SQLiteDatabase dbw;
    /**
     * Begin the dataset-update transaction.
     * @author cav94mat
     */
    public void datasetBegin() {
        dbw = this.getWritableDatabase();
        dbw.beginTransaction();
    }

    /**
     * Get the current local-dataset version.
     */
    public long datasetVersion() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM userprefs WHERE id = 'dataset_version'", null);
        if (c.getCount() > 0) {
            c.moveToFirst();
            return Long.parseLong(c.getString(c.getColumnIndex("value")));
        }
        else
            return 0;
    }
    /*
     * End the current dataset-update transaction.
     * @param newVer   Updated dataset version.
     * @author cav94mat
     */
    public void datasetEnd(long version) {
        if (dbw != null) {
            if (version > 0) {
                ContentValues cols = new ContentValues();
                cols.put("value", Long.toString(version));
                dbw.update("userprefs", cols, "id = 'dataset_version'", null);
                dbw.setTransactionSuccessful();
            }
            dbw.endTransaction();
            dbw.close();
            dbw = null;
        }
    }
    /*
     * End the current dataset-update transaction, aborting it.
     * @param success   True to record changed, false to cancel them.
     * @author cav94mat
     */
    public void datasetEnd() {
        this.datasetEnd(0);
    }
    /**
     * Update or insert an additive into the local dataset.
     * @param id        Additive identifier.
     * @param name      Additive display name.
     * @param eNumber   Additive E-number.
     * @throws NullPointerException if datasetBegin() hadn't been called before.
     * @author cav94mat
     */
    public void datasetAdditive(String id, String name, String eNumber) {
        ContentValues cols = new ContentValues();
        cols.put("additiveName", name);
        cols.put("approvation", 0);
        cols.put("approvationDate", 0);
        if (!eNumber.isEmpty())
            cols.put("eNumber", eNumber);
        // Update existing, or insert new
        if (dbw.update("additives", cols, "id = ?", new String[]{id}) <= 0) {
            cols.put("id", id);
            dbw.insertOrThrow("additives", null, cols);
        }
    }
    /**
     * Register an application for an additive, if the new approval status is higher than the memorized one.
     * @param additiveId        Additive identifier.
     * @param approvalStatus    (10 * applicationStatus) + approvalStatus;
     *                          applicationStatus is a number from 0 (application initiated) to 6 (application completed).
     *                          approvalStatus is either 0 (unknown), 1 (positive) or 2 (negative).
     * @param approvalDate      Approvation or rejection date, expressed as millisecs since UNIX epoch.
     * @throws NullPointerException if datasetBegin() hadn't been called before.
     * @author cav94mat
     */
    public void datasetApplication(String additiveId, byte approvalStatus, long approvalDate) {
        ContentValues cols = new ContentValues();
        cols.put("approvation", approvalStatus);
        cols.put("approvationDate", approvalDate);
        // Update or fail
        if (dbw.update("additives", cols, "id = ? AND approvation < ?", new String[]{additiveId, Byte.toString(approvalStatus)}) <= 0)
            throw new IllegalArgumentException("No row to update.");
    }

//</editor-fold>
    /**
     * Obtain additive from name.
     * @param name Known additive name.
     * @return Cursor object
     */
    public Cursor getAdditiveByName(String name) {
        return getReadableDatabase().rawQuery("SELECT * FROM additives WHERE UPPER(additiveName) =?", new String[]{ name.toUpperCase()});
    }

    /**
     * Obtain additive from name.
     * @param name Known additive name and limit of result
     * @return Cursor object
     */
    public Cursor getAdditiveByNameWithLimit(String name, int limit){
        boolean isENum = false;
        //verifico se name è un nome di un ingrediente oppure il codice di un ingrediente
        for (int i = 0; i < name.length() && !isENum; i++){
            if (name.charAt(i) >= '0' && name.charAt(i) <= '9')
                isENum = true;
        }
        if(isENum)
            return getNameAdditive(name, limit);
        else
            return getReadableDatabase().rawQuery("SELECT DISTINCT id, additiveName, eNumber, approvation, approvationDate FROM additives WHERE UPPER(additiveName) LIKE ? LIMIT ?", new String[]{ (name.toUpperCase() + "%"), Integer.toString(limit) });
    }
    public Cursor getAdditiveContentNameWithLimit(String name, int limit){
        return getReadableDatabase().rawQuery("SELECT * FROM additives WHERE UPPER(additiveName) LIKE ? LIMIT ?", new String[]{ ("%" + name.toUpperCase() + "%"), Integer.toString(limit) });
    }

    public Cursor getRequestById(int dbEntry){
        return getReadableDatabase().rawQuery("SELECT * FROM requests WHERE id = ?", new String[]{ Integer.toString(dbEntry) });
    }
    public Cursor getResponseByIdRequest(int idReq){
        return getReadableDatabase().rawQuery("SELECT * FROM responses WHERE req = ?", new String[]{ Integer.toString(idReq) });
    }
    public Cursor getResponseById(int idRes){
        return getReadableDatabase().rawQuery("SELECT * FROM responses WHERE id = ?", new String[]{ Integer.toString(idRes) });
    }
    //inserisce una nuova riga nella tabella requests avendo come paramentro un oggetto Request
    public int insertRequest(Request request) {
        ContentValues cols = new ContentValues();
        cols.put("initDate", request.getTimestamp());;
        this.getWritableDatabase().insertOrThrow("requests", null, cols);
        return request.dbEntry;
    }
    //Dato un eNum restituisce la riga relativa all'additive
    public Cursor getNameAdditive(String eNum, int limit){
        //  E<numero> (es. "E754")
        //  E-<numero> (es. "E-754")
        int i;
        for(i = 0; !Character.isDigit(eNum.charAt(i)); i++);
        eNum = eNum.substring(i);
        return getReadableDatabase().rawQuery("SELECT DISTINCT id, additiveName, eNumber, approvation, approvationDate FROM additives WHERE UPPER(eNumber) LIKE ? LIMIT ?", new String[] { ("%" + eNum.toUpperCase() + "%"), Integer.toString(limit) });
    }

    //Data una subquery verifica se esiste un additive correllato
    public Cursor getAdditiveBySubquery(String subquery){
        return getReadableDatabase().rawQuery("SELECT * FROM additives WHERE UPPER(eNumber) = ? OR UPPER(additiveName) = ?", new String[]{ subquery.toUpperCase(), subquery.toUpperCase()} );
    }

    //inserisce una nuova riga nella tabella requests avendo come paramentro l'id e il tempo
    public long insertRequestWithParam(long time) {
        ContentValues cols = new ContentValues();
        cols.put("initDate", time);;
        return this.getWritableDatabase().insertOrThrow("requests", null, cols);
        //return cols.getAsInteger("id");
        //return this.getWritableDatabase().rawQuery("INSERT INTO requests (initDate) VALUES (?)", new String[]{ Long.toString(time)});
    }

    public Cursor getLastResponse(int inizio, int limit){
        return getReadableDatabase().rawQuery("SELECT * FROM responses ORDER BY lastTimestamp DESC LIMIT ? OFFSET ?", new String[]{ Integer.toString(limit), Integer.toString(inizio)});
    }
}