package it.malg.crimson.model;

import android.database.Cursor;
import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Represent a substance.
 */
public class Additive {
// <editor-fold desc="Membri statici">
    private static final int cacheMax = 128;
    // Cache-system to prevent excessive data-set querying.
    private static ArrayList<Additive> cache = new ArrayList<>();
    /** Add to dataset. If cache size outgrows cacheMax, it will delete the oldest element. */
    private static Additive cacheInsert(Additive element) {
        cache.ensureCapacity(cacheMax);
        cache.add(0, element);
        if (cache.size() > cacheMax)
            cache.remove(cacheMax - 1);
        return element;
    }
    /**
     * Initialize an Additive instance, by knowing its name.
     * @param name The name of the additive you want to get.
     * @return Initialized (or cached) instance.
     * @throws IllegalArgumentException when the supplied eNumber isn't found in the dataset.
     * @throws IllegalStateException if the local dataset isn't ready.
     */
    public static Additive fromName(String name) throws IllegalArgumentException, IllegalStateException {
        for (Additive a : cache)
            if (a.name.equals(name))
                return a;
        return cacheInsert(new Additive(name));
    }
    /**
     * Obtain all additives whose name starts with or contains "query".
     * @param query String to look for.
     * @param limit Maximum amount of additives to retrieve.
     * @return At most `limit` additives
     */
    public static Additive[] search(String query, int limit) {
        ArrayList<Additive> out = new ArrayList<>();
        /* TODO: Interroga il Database.
         * Dai prima i risultati che iniziano per $query, poi quelli che contengono $query nel
         * loro display-name. Non superare $limit risultati.
         */
        if(query.length() != 0) {
            Cursor c = Database.getInstance().getAdditiveByNameWithLimit(query, limit);
            if (c != null && (c.getCount() > 0))
            //aggiungo i primi limit additive all'arraylist cercati per l'iniziale del nome
                try {
                    if (c.moveToFirst()) {
                        do {
                            Additive addit = new Additive();
                            addit.name = c.getString(c.getColumnIndex("additiveName"));
                            addit.eNumber = c.getString(c.getColumnIndex("eNumber"));
                            addit.approvalTimestamp = c.getLong(c.getColumnIndex("approvationDate"));
                            switch (c.getInt(c.getColumnIndex("approvation")) % 10) {
                                case 1:
                                    addit.approval = eApprovalStatus.Approved;
                                    break;
                                case 2:
                                    addit.approval = eApprovalStatus.Rejected;
                                    break;
                                default:
                                    addit.approval = eApprovalStatus.Unknown;
                            }
                            // adding to out list
                            out.add(addit);
                        } while (c.moveToNext());
                    }
                } finally {
                    c.close();
                }
        }
        return out.toArray(new Additive[out.size()]);
    }
// </editor-fold>
    private String name;
    private String eNumber;
    private long approvalTimestamp;
    private eApprovalStatus approval;
    private Additive() throws IllegalStateException{
        if (! Dataset.getInstance().isReady())
            throw new IllegalStateException("Dataset not ready!");
    }
    /**
     * @see #fromName(String)
     */
    private Additive(String name) throws IllegalArgumentException, IllegalStateException  {
        this();
        Cursor db = Database.getInstance().getAdditiveByName(name);
        try {
            if (db.moveToFirst()) {
                this.name = db.getString(db.getColumnIndex("additiveName"));
                this.eNumber = db.getString(db.getColumnIndex("eNumber"));
                this.approvalTimestamp = db.getLong(db.getColumnIndex("approvationDate"));
                switch (db.getInt(db.getColumnIndex("approvation")) % 10) {
                    case 1:
                        approval = eApprovalStatus.Approved; break;
                    case 2:
                        approval = eApprovalStatus.Rejected; break;
                    default:
                        approval = eApprovalStatus.Unknown;
                }
            } else
                throw new IllegalArgumentException(String.format("Not found: %s", name));
        } finally {
            db.close();
        }
    }
    /**
     * Get the approval status of this instance.
     * @return Whether the substance was approved, banned or not yet tested by an EU commission.
     */
    public eApprovalStatus getApprovalStatus() {
        return this.approval;
    }
    /**
     * Get the name associated to this additive instance.
     * @return The substance display name.
     */
    public String getName() {
        return this.name;
    }
    /**
     * Get the E-number associated to this additive instance, or zero if not defined.
     * @return The E-number, or zero if unspecified.
     */
    public String getENumber() {
        return this.eNumber;
    }

    /**
     * Get the timestamp associated to this additive approvation/rejection by the EU commission.
     * @return Millisecs between UNIX epoch and this additive's approvation/rejection.
     */
    public long getApprovalTimestamp() {
        return this.approvalTimestamp;
    }
    /**
     * Get an URL to a page with more information about the present additive, if available.
     */
    public String getDocument() {
        try {
            return String.format("https://malg-it.bitbucket.io/crimson/doc/name=%s", URLEncoder.encode(this.getName(), "utf-8"));
        } catch (UnsupportedEncodingException ex) {
            Log.e("Additive.getDocument", "Unsupported URL encoding!", ex);
            return "";
        }
    }
    /**
     * Get the name and E-number together in the same string, for displaying and searching purposes.
     * @return e.g. "Additive (E123)"
     */
    public String getDisplayName() {
        return String.format(Locale.ENGLISH, "%s (E%s)", this.getName(), this.getENumber());
    }
    /**
     * Represent the approval status of a substance.
     */
    public enum eApprovalStatus {
        /** The substance hadn't been rated yet. */
        Unknown,
        /** The substance was approved by an EU commission. */
        Approved,
        /** The substance was banned by an EU commission. */
        Rejected
    }
}