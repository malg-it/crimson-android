package it.malg.crimson.model;

import android.database.Cursor;

import java.util.ArrayList;

/**
 * Represent a scan response.
 */
public class Response {
    protected Request request;
    protected long lastTimestamp;
    protected int dbEntry;
    protected String subQuery;
    protected Additive additive;
    protected eStatus status = eStatus.Pending;
    protected Additive.eApprovalStatus approval = Additive.eApprovalStatus.Unknown;
    protected Additive.eApprovalStatus originalApproval = Additive.eApprovalStatus.Unknown;
    protected Response() { // costruttore comune
        if (! Dataset.getInstance().isReady())
            throw new IllegalStateException("Dataset not ready.");
    }

    /**
     * Initialize a new response, given a request and part of a user-supplied query.
     * @param request   Original request.
     * @param subQuery  User-provided query substring, relative to a single additive name or E-number.
     *                  (e.g. 'curcumin', 'e754', 'e-754')
     */
    public Response(Request request, String subQuery) {
        this();
        this.subQuery = subQuery;//.toLowerCase();
        // TODO: Creare entità Request nel db e memorizzare ID associato.
        this.request = request;

        //TODO: Da stringa (subquery) a Additive
        // Usa this.subQuery (sempre minuscolo). Formati possibili:
        //  <nomeIngrediente> (es. "curcumin")
        //  E<numero> (es. "E754")
        //  E-<numero> (es. "E-754")
        // Utilizzare il costruttore della classe Additive per recuperare un ingrediente per nome
        // o per E-number dopo opportuna conversione ad int.
        boolean isENum = false;
        Additive adt;
        //verifico se subquery è un nome di un ingrediente oppure il codice di un ingrediente
        for (int i = 0; i < this.subQuery.length() && !isENum; i++){
            if (this.subQuery.charAt(i) >= '0' && this.subQuery.charAt(i) <= '9')
                isENum = true;
        }
        int limit = Database.getInstance().getAdditiveBySubquery(this.subQuery).getCount();
        if (limit > 0) { //TODO: Modifica la condizione: Se la subquery può essere correlata ad un Additive...
            if (isENum) {
                Cursor c = Database.getInstance().getNameAdditive(this.subQuery, limit);
                c.moveToFirst();
                adt = Additive.fromName(c.getString(c.getColumnIndex("additiveName")));
            }
            else {
                adt = Additive.fromName(this.subQuery);
            }
            this.status = eStatus.Successful;
            this.additive = adt;
        } else
            this.status = eStatus.NotFound;
        this.dbEntry = Database.getInstance().insertRequest(request);
        // Generates the result
        this.retry();
    }

    /**
     * Load a response instance from the Database.
     * @param id    Database synthetic key.
     */
    public Response(int id) {
        this();
        this.dbEntry = id;
        Cursor c = Database.getInstance().getResponseById(id);
        if (c.moveToFirst()) { // TODO: Modificare la condizione: Esiste la risposta 'dbEntry' nel Database.
            // TODO: Ottenere i dati dal db
            this.subQuery = c.getString(c.getColumnIndex("subquery"));
            switch (c.getInt(c.getColumnIndex("status"))){
                case 0:
                    this.status = eStatus.Pending; break;
                case 1:
                    this.status = eStatus.Successful; break;
                case 2:
                    this.status = eStatus.NotFound; break;
            }
            switch (c.getInt(c.getColumnIndex("lastResult")) % 10) {
                case 1:
                    this.approval = Additive.eApprovalStatus.Approved; break;
                case 2:
                    this.approval = Additive.eApprovalStatus.Rejected; break;
                default:
                    this.approval = Additive.eApprovalStatus.Unknown;
            }
            switch (c.getInt(c.getColumnIndex("origResult")) % 10) {
                case 1:
                    this.originalApproval = Additive.eApprovalStatus.Approved; break;
                case 2:
                    this.originalApproval = Additive.eApprovalStatus.Rejected; break;
                default:
                    this.originalApproval = Additive.eApprovalStatus.Unknown;
            }
            this.lastTimestamp = c.getLong(c.getColumnIndex("lastTimestamp"));
            this.request = new Request(c.getInt(c.getColumnIndex("req")));
            if (this.getResponseStatus() == eStatus.Successful)
                 this.additive = Additive.fromName(c.getString(c.getColumnIndex("additive")));
        } else
            throw new IllegalArgumentException(String.format("Request ID not found: %d", dbEntry));
    }

    public static ArrayList<Response> getLatest(int offset, int count) {
        //cursor che contiene tutti i response ordinati in ordine decrescente dal lastTimestamp
        Cursor c = Database.getInstance().getLastResponse(offset, count);
        ArrayList<Response> ress = new ArrayList<Response>();
        c.moveToFirst();
        //mi sposto con il cursor finchè arrivo ad offset
        if (c.getCount() > 0) {
            do {
                ress.add(new Response(c.getInt(c.getColumnIndex("id"))));
                count--;
            }
            while (c.moveToNext() && count >= 0);
        }
        return ress;
    }
    /**
     * Recalculate the approval status of this response, updating the timestamp as well.
     */
    public void retry() {
        this.lastTimestamp = this.lastTimestamp > 0 ? System.currentTimeMillis() : getOriginalTimestamp();
        if (this.getResponseStatus() == eStatus.Successful) {
            this.approval = this.getAdditive().getApprovalStatus();
            if (this.originalApproval == Additive.eApprovalStatus.Unknown)
                this.originalApproval = this.approval;
        }
    }

    /**
     * Get the original user-provided query string.
     * @return
     */
    public String getOriginalQuery() {
        return this.subQuery;
    }

    /**
     * Get the response status.
     * @return
     */
    public eStatus getResponseStatus() {
        return this.status;
    }

    /**
     * Get the additive matching the present response, or null if no match has been found (yet).
     * @see #getResponseStatus()
     */
    public Additive getAdditive() {
        return this.additive;
    }
    /**
     * Get the original request timestamp.
     * @see #getTimestamp()
     */
    public long getOriginalTimestamp() {
        return this.request.getTimestamp();
    }

    /**
     * Get the timestamp of the last recalculation.
     * @see #retry()
     * @see #getOriginalTimestamp()
     */
    public long getTimestamp() {
        return this.lastTimestamp;
    }
    /**
     * Get the approval status for the matched additive at the time of the last recalculation.
     * @see #getAdditive()
     * @see #getOriginalApprovalStatus()
     * @see #retry()
     */
    public Additive.eApprovalStatus getApprovalStatus() {
        return this.approval;
    }
    /**
     * Get the approval status for the matched additive at the time of the first calculation.
     * @see #getAdditive()
     * @see #getApprovalStatus()
     */
    public Additive.eApprovalStatus getOriginalApprovalStatus() {
        return this.originalApproval;
    }
    /**
     * Possible response statuses.
     */
    public enum eStatus {
        /** The response hasn't been calculated yet. */
        Pending,
        /** The response was calculated successfully. */
        Successful,
        /** The response calculation failed because no matching additive has been found. */
        NotFound,
    }
}
