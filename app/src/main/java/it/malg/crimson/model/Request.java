package it.malg.crimson.model;

import android.database.Cursor;

import java.util.ArrayList;

/**
 * Represent a scan request.
 */
public class Request {
    int dbEntry;
    protected long timestamp;
    protected String[] query;
    protected ArrayList<Response> responses;
    protected Request() {
        if (! Dataset.getInstance().isReady())
            throw new IllegalStateException("Dataset not ready.");
    }
    /**
     * Initialize a request.
     * @param query User-provided strings to look for.
     */
    public Request(String[] query) {
        this();
        this.timestamp = System.currentTimeMillis();
        this.query = query;
        // TODO: Creare entità Request nel db e memorizzare ID associato.
        this.responses = new ArrayList<Response>();
        for (int i = 0; i < query.length; i++) {
            this.responses.add(new Response(this, query[i]));
        }
        this.dbEntry = (int) Database.getInstance().insertRequestWithParam(this.timestamp);
    }
    /**
     * Load a request instance from the Database.
     * @param dbEntry Database synthetic key.
     * @throws IllegalArgumentException if dbEntry isn't defined in the db.
     */
    public Request(int dbEntry) throws IllegalArgumentException {
        this();
        this.dbEntry = dbEntry;
        Cursor c = Database.getInstance().getRequestById(dbEntry);
        Cursor cReq = Database.getInstance().getResponseByIdRequest(dbEntry);
        if (c.moveToFirst()) { // TODO: Modificare la condizione: Esiste la richiesta 'dbEntry' nel Database.
            // TODO: Ottenere i dati dal db
            this.timestamp = c.getLong(c.getColumnIndex("initDate"));
            this.responses = new ArrayList<Response>();
            if(cReq.moveToFirst())
                do {
                    this.responses.add(new Response(cReq.getInt(cReq.getColumnIndex("id"))));
                } while (cReq.moveToNext());
            //this.query = c.getString(c.getColumnIndex("query"));
        } else
            throw new IllegalArgumentException(String.format("Request ID not found: %d", dbEntry));
    }
    /**
     * Perform the responses generation again.
     */
    public void retryAll() {
        for (Response r: this.responses)
            r.retry();
    }
    /**
     * Get the query substrings provided by the user.
     * @return
     */
    public String[] getQuery() {
        return this.query;
    }

    /**
     * Get the responses associated to this instance.
     */
    public Response[] getReponses() {
        return this.responses.toArray(new Response[responses.size()]);
    }
    /**
     * Get the timestamp taken when the request was initialized.
     */
    public long getTimestamp() {
        return this.timestamp;
    }
}
