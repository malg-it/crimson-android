package it.malg.crimson.model;

import android.os.Environment;
import android.util.JsonReader;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Enumeration;
import java.util.Locale;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.net.ssl.HttpsURLConnection;

import it.malg.crimson.model.exceptions.DatasetUpdateException;

/**
 * Point of access to the dataset.
 * @author cav94mat
 */
public final class Dataset {
    private Dataset() {
        this.setURL("https://circabc.europa.eu/d/a/workspace/SpacesStore/2387736c-833c-49be-8f08-f72e297bf3fd/FoodAdditives.zip");
    }
    private static Dataset instance = new Dataset();
    /**
     * Get Singleton's instance.
     */
    public static Dataset getInstance() {
        return instance;
    }
    // URL
    private String url = "";
    private HttpsURLConnection openRemote() throws IOException {
        HttpsURLConnection conn = (HttpsURLConnection) new URL(this.getURL()).openConnection();
        if (conn.getResponseCode() != 200)
            throw new IOException(String.format("HTTP error %d (%s).", conn.getResponseCode(), conn.getResponseMessage()));
        //conn.setRequestMethod("GET");
        //conn.setDoInput(true);
        //conn.connect();
        return conn;
    }
    // Pull
    /**
     * Download the dataset from the specified URL.
     * @param forced Whether to force san update even if the local and remote timestamps match.
     * @return True: download performed, False: already up-to-date (and forced == false).
     * @throws IOException if transfer errors occur.
     * @author cav94mat
     */
    public boolean pull(boolean forced) throws DatasetUpdateException {
        class Utils {
            public boolean deleteContents(File dir) {
                File[] files = dir.listFiles();
                boolean success = true;
                if (files != null) {
                    for (File file : files) {
                        if (file.isDirectory()) {
                            success &= deleteContents(file);
                        }
                        if (!file.delete()) {
                            Log.w("Dataset.pull", "Failed to delete " + file);
                            success = false;
                        }
                    }
                }
                return success && dir.delete();
            }
            public void pipe(InputStream in, OutputStream out) throws IOException {
                int len = -1;
                byte[] buffer = new byte[1024];
                while ((len = in.read(buffer)) != -1)
                    out.write(buffer, 0, len);
                in.close();
                out.close();
            }
        }
        // Get DB instance
        Database db = Database.getInstance();
        try {
            HttpsURLConnection conn = this.openRemote();
            Utils utils = new Utils();
            long newVer = this.getRemoteVersion(conn);
            if (forced || !(this.isReady() && this.getLocalVersion() >= newVer)) {
                Log.i("Dataset.pull", String.format("Pulling latest dataset from remote (%s)...", newVer));
                // Download .zip
                File tmpDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/.crimson");
                if (!((!tmpDir.isDirectory() || utils.deleteContents(tmpDir)) && tmpDir.mkdir()))
                    throw new DatasetUpdateException("Cache initialization failed.");
                File tmpDataset = new File(String.format("%s/eu-dataset.%s.zip", tmpDir.getAbsolutePath(), newVer));
                try {
                    utils.pipe(conn.getInputStream(), new FileOutputStream(tmpDataset));
                } catch (Exception ex) {
                    Log.e("Dataset.pull", "Download failed", ex);
                    throw new DatasetUpdateException("Download failed.", ex);
                } finally {
                    conn.disconnect();
                }
                // Extract .zip
                Log.i("Dataset.pull", String.format("Extracting: %s", tmpDataset.getAbsolutePath()));
                try {
                    ZipFile zf = new ZipFile(tmpDataset);
                    Enumeration<? extends ZipEntry> zee = zf.entries();
                    while (zee.hasMoreElements()) {
                        ZipEntry ze = zee.nextElement();
                        String name = tmpDir.getAbsolutePath() + "/" + ze.getName();
                        if (ze.isDirectory())
                            new File(name).mkdirs();
                        else
                            utils.pipe(zf.getInputStream(ze), new FileOutputStream(name));
                    }
                    zf.close();
                    tmpDataset.delete();
                } catch (Exception ex) {
                    Log.e("Dataset.pull", "Extraction failed", ex);
                    throw new DatasetUpdateException("Extraction failed.", ex);
                }
                // Init database transaction
                db.datasetBegin();
                // DB update/population
                Log.i("Dataset.pull", "Importing additives...");
                String url = "http://ec.europa.eu/semantic_webgate/dataset/additives/resource";
                SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S", Locale.ENGLISH); // "EEE MMM d HH:mm:ss zzz yyyy");
                // 1. Additives
                InputStream in = new FileInputStream(tmpDir.getAbsolutePath() + "/additives.rdf.additive.json");
                JsonReader json = new JsonReader(new InputStreamReader(in));
                int count = 0;
                try {
                    json.beginObject();
                    if (json.nextName().equals("items")) {
                        json.beginArray();
                        while (json.hasNext()) {
                            json.beginObject();
                            boolean passing = true;
                            // Vars
                            String addId = "";
                            String addName = "";
                            String addENumber = "";
                            while (json.hasNext()) {
                                try {
                                    String name = json.nextName();
                                    if (name.equals("@subject"))
                                        addId = json.nextString();
                                    else if (name.equals(url + "/additiveName"))
                                        addName = json.nextString();
                                    else if (name.equals(url + "/eNumber"))
                                        addENumber = json.nextString().substring(2);
                                    else if (name.equals(url + "/isGroup")) {
                                        if (!json.nextString().equalsIgnoreCase("no"))
                                            passing = false; // Reject groups
                                    } else
                                        json.skipValue();
                                } catch (Exception ex) {
                                    Log.w("Dataset.pull.additive", "Skipping invalid field.", ex);
                                    // Skip field and mark entire item as invalid
                                    json.skipValue();
                                    passing = false;
                                }
                            }
                            json.endObject();
                            if (passing && !addName.isEmpty()) {
                                try {
                                    db.datasetAdditive(addId, addName, addENumber);
                                    Log.i("Dataset.pull.additive", String.format("Additive: %s (E %s)", addName, addENumber));
                                    count++;
                                } catch (Exception ex) {
                                    Log.w("Dataset.pull.additive", String.format("Error inserting %s (%s)", addName, addENumber), ex);
                                }
                            }
                        }
                        json.endArray();
                    }
                    json.endObject();
                } finally {
                    json.close();
                    in.close();
                }
                if (count > 0)
                    Log.i("Dataset.pull", count + " additive(s) found!");
                else
                    throw new DatasetUpdateException("No additive(s) found.");
                // 2. Applications
                Log.i("Dataset.pull", "Importing applications...");
                in = new FileInputStream(tmpDir.getAbsolutePath() + "/additives.rdf.application.json");
                json = new JsonReader(new InputStreamReader(in));
                count = 0;
                try {
                    json.beginObject();
                    if (json.nextName().equals("items")) {
                        json.beginArray();
                        while (json.hasNext()) {
                            json.beginObject();
                            boolean passing = true;
                            // Vars
                            String appAddId = ""; // Additive id
                            long appDate = 0;
                            byte appStatus = 0;
                            byte appApproval = 0; //Additive.eApprovalStatus.Unknown;
                            while (json.hasNext()) {
                                try {
                                    String name = json.nextName();
                                    if (name.equals(url + "/applicationStatus")) {
                                        String apStatusRaw = json.nextString();
                                        if (apStatusRaw.equalsIgnoreCase("procedure initiated") || apStatusRaw.equalsIgnoreCase("application initiated"))
                                            appStatus = 1;
                                        else if (apStatusRaw.equalsIgnoreCase("procedure received") || apStatusRaw.equalsIgnoreCase("application received"))
                                            appStatus = 2;
                                        else if (apStatusRaw.equalsIgnoreCase("procedure accepted") || apStatusRaw.equalsIgnoreCase("application accepted"))
                                            appStatus = 3;
                                        else if (apStatusRaw.equalsIgnoreCase("risk assessment ongoing"))
                                            appStatus = 4;
                                        else if (apStatusRaw.equalsIgnoreCase("EFSA opinion adopted"))
                                            appStatus = 5;
                                        else if (apStatusRaw.equalsIgnoreCase("procedure completed") || apStatusRaw.equalsIgnoreCase("application completed"))
                                            appStatus = 6;
                                        else
                                            passing = false;
                                    } else if (name.equals(url + "/hasAdditive")) {
                                        if ((appAddId = json.nextString()).isEmpty())
                                            passing = false;
                                    } else if (name.equals(url + "/decisionDate"))
                                        appDate = parser.parse(json.nextString()).getTime();
                                    else if (name.equals(url + "/decision")) {
                                        String approval = json.nextString();
                                        if (approval.equalsIgnoreCase("positive"))
                                            appApproval = 1;
                                        else if (approval.equalsIgnoreCase("negative"))
                                            appApproval = 2;
                                        else
                                            passing = false; // Skip uncertain applications
                                    }
                                    else
                                        json.skipValue();
                                } catch (Exception ex) {
                                    Log.w("Dataset.pull.appl", "Skipping invalid field.", ex);
                                    // Skip field and mark entire item as invalid
                                    json.skipValue();
                                    passing = false;
                                }
                            }
                            appApproval += appStatus * 10;
                            json.endObject();
                            if (passing && !appAddId.isEmpty()) {
                                try {
                                    db.datasetApplication(appAddId, appApproval, appDate);
                                    Log.i("Dataset.pull.appl", String.format("Application for: %s (approval=%d, date=%s)", appAddId, appApproval, appDate));
                                    count++;
                                } catch (Exception ex) {
                                    Log.w("Dataset.pull.appl", String.format("Error importing application for %s", appAddId), ex);
                                }
                                //db.setAdditiveApproval(ap, a, apApproval, apDate, apDocRef);
                            }
                        }
                        json.endArray();
                    }
                } finally {
                    json.close();
                    in.close();
                }
                if (count > 0)
                    Log.i("Dataset.pull", count + " application(s) found!");
                else
                    Log.w("Dataset.pull", "No application(s) valid for importation!");
                db.datasetEnd(newVer);
                return true;
            } else
                return false;
        } catch (DatasetUpdateException ex) {
            db.datasetEnd();
            Log.e("Dataset.pull", ex.getMessage(), ex);
            throw ex;
        } catch (Exception ex) {
            db.datasetEnd();
            Log.e("Dataset.pull", "Unexpected error", ex);
            throw new DatasetUpdateException("Unexpected error.", ex);
        }
    }

    /**
     * Update the local dataset downloading it from the specified URL.
     * @return True: download performed, False: already up-to-date.
     */
    public boolean pull() throws DatasetUpdateException {
        return this.pull(false);
    }
    /**
     * Set the dataset download URL.
     * @param url New URL for the dataset download.
     */
    public void setURL(String url) {
        this.url = url;
    }
    /**
     * Get the dataset download URL.
     * @return The URL currently set for the dataset download.
     */
    public String getURL() {
        return this.url;
    }
    // Local dataset
    /**
     * Return the version of the local copy of the dataset.
     * @return 0: not acquired, >0: current version
     */
    public long getLocalVersion() {
        return Database.getInstance().datasetVersion();
    }

    /**
     * Return the latest version of the dataset available from remote.
     * @throws IOException If a transfer error occurs.
     */
    public long getRemoteVersion() throws IOException {
        return getRemoteVersion(this.openRemote());
    }
    private long getRemoteVersion(HttpsURLConnection conn) throws IOException {
        long ret = conn.getLastModified();
        return (ret > 0) ? ret : conn.getDate(); // Required for load testing
    }
    /**
     * Whether the dataset has already been downloaded locally.
     */
    public boolean isReady() {
        return this.getLocalVersion() > 0;
    }
}
