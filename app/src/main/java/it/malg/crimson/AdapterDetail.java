package it.malg.crimson;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.List;

/*Adapter,rv per card dettaglio*/

public class AdapterDetail extends RecyclerView.Adapter<AdapterDetail.SostanzaViewHolder> {

    private List<Sostanza> sostanzaList;

    public AdapterDetail(List<Sostanza> cards) {
        this.sostanzaList = cards;
    }

    @Override
    public SostanzaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card, parent, false);;

        return new SostanzaViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SostanzaViewHolder holder, int position) {
        Sostanza ind = sostanzaList.get(position);
        holder.nomeText.setText(ind.getNome());
        holder.responsoText.setText(ind.getResponso());
        holder.dataAcquisizioneText.setText(ind.getDataAcquisizione());
        holder.dataControlloText.setText(ind.getDataControllo());
        holder.categoriaText.setText(ind.getCategoria());
        holder.subCategoriaText.setText(ind.getSubCategoria());
        holder.dataDatasetText.setText(ind.getDataDataset());
        holder.pallinoImage.setColorFilter(ind.getColore());
        holder.document=ind.getDocument();
    }
    @Override
    public int getItemCount() {
        return sostanzaList.size();
    }

    public static class SostanzaViewHolder extends RecyclerView.ViewHolder {
        TextView nomeText;
        TextView responsoText;
        TextView dataAcquisizioneText;
        TextView dataControlloText;
        TextView categoriaText;
        TextView subCategoriaText;
        TextView dataDatasetText;
        ImageView pallinoImage;
        String document;

        public SostanzaViewHolder(View itemView) {
            super(itemView);
            nomeText = (TextView) itemView.findViewById(R.id.nome_sostanza);
            responsoText = (TextView) itemView.findViewById(R.id.responso);
            dataAcquisizioneText = (TextView) itemView.findViewById(R.id.data);
            dataControlloText = (TextView) itemView.findViewById(R.id.ultimo_controllo);
            categoriaText = (TextView) itemView.findViewById(R.id.categoria);
            subCategoriaText = (TextView) itemView.findViewById(R.id.subcategoria);
            dataDatasetText = (TextView) itemView.findViewById(R.id.ultimaIndagine);
            pallinoImage = (ImageView) itemView.findViewById(R.id.simbolo_responso);

            GridLayout lineInfo = (GridLayout) itemView.findViewById(R.id.line_info);
            GridLayout lineWiki = (GridLayout) itemView.findViewById(R.id.line_wikipedia);
            GridLayout lineCor  = (GridLayout) itemView.findViewById(R.id.line_correlate);
            lineInfo.setOnClickListener(mListener);
            lineWiki.setOnClickListener(mListener);
            lineCor.setOnClickListener(mListener);
        }

        private View.OnClickListener mListener = new View.OnClickListener() {
            Intent intent;
            String url;
            public void onClick(View v) {
                switch(v.getId()){
                    case R.id.line_info :       intent = new Intent(Intent.ACTION_VIEW, Uri.parse(document));
                                                v.getContext().startActivity(intent);
                        break;

                    case R.id.line_wikipedia:   String nomeUrl = (String) nomeText.getText();
                                                url = "https://en.wikipedia.org/wiki/"+nomeUrl.replaceAll("\\(.*\\)", "");
                                                intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                                                v.getContext().startActivity(intent);
                        break;
                    case R.id.line_correlate:   Context context = v.getContext();
                                                ((Activity) context).finish();
                        break;
                }
            }
        };
    }
}
