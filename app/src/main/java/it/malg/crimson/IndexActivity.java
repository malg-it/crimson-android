package it.malg.crimson;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewStub;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SearchView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import it.malg.crimson.model.Additive;
import it.malg.crimson.model.Database;
import it.malg.crimson.model.Request;
import it.malg.crimson.model.Response;

public class IndexActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, SearchView.OnQueryTextListener  {

    ListView list;
    ListViewAdapter adapter;
    ArrayList<Sostanza> mainData = new ArrayList<>();
    ArrayList<Response> r;
    List<String> testVerifiedList = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_index);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        list = (ListView) findViewById(R.id.listview);

        ProgressBar progressBar = (ProgressBar) findViewById(R.id.LoadingSubstance);
        progressBar.setVisibility(View.GONE);

        SearchView searchView = (SearchView) findViewById(R.id.searchView);
        searchView.setOnQueryTextListener(this);
        searchView.setIconifiedByDefault(false);


        r = Response.getLatest(0,10);
        if (r.size()<1) {
            Random r = new Random();
            char c = (char) (r.nextInt(26) + 'a');
            searchView.setQuery(""+c,false);
            getResultCard(""+c);
        }else{
            getHistoryCard(0,10);
        }
    }

    void getHistoryCard(int offset, int count){
        r = Response.getLatest(offset, count);
        int color = getResources().getColor(R.color.colorNotFound);

        for (int i = 0; i < r.size(); i++) {
            if (r.get(i).getAdditive().getApprovalStatus().toString() == "Unknown") {
                color = getResources().getColor(R.color.colorUnknown);
            } else if (r.get(i).getAdditive().getApprovalStatus().toString() == "Approved") {
                getResources().getColor(R.color.colorAccepted);
            } else if (r.get(i).getAdditive().getApprovalStatus().toString() == "Rejected") {
                color = getResources().getColor(R.color.colorReject);
            }
            Sostanza substanceNames = new Sostanza(r.get(i).getAdditive().getDisplayName().toString(),r.get(i).getAdditive().getApprovalStatus().toString(),"",Long.toString(r.get(i).getAdditive().getApprovalTimestamp())," "," ","DD/MM/YY",r.get(i).getAdditive().getDocument(),color);
            mainData.add(substanceNames);
        }
        adapter = new ListViewAdapter(this, mainData);
        list.setAdapter(adapter);
    }

    void getResultCard(String query){
        mainData.clear(); //delete previous
        testVerifiedList.removeAll(testVerifiedList);
        String x[] = {query};
        Additive additives[] = Additive.search(x[0],10);
        int p=0;
        while(p<additives.length){
            testVerifiedList.add(0,additives[p].getName());
            p++;
        }
        String[] testVerifiedArray = new String[testVerifiedList.size()];
        testVerifiedArray = testVerifiedList.toArray(testVerifiedArray);
        boolean insert=false;
        Request request = new Request(testVerifiedArray);
        for(int jj=0;jj<testVerifiedArray.length;jj++) {
            if (Database.getInstance().getAdditiveBySubquery(testVerifiedArray[jj]).getCount() > 0 && testVerifiedArray[jj]!=null) {

                Response reponses[] = request.getReponses();

                int color= getResources().getColor(R.color.colorAccepted);
                if (reponses[jj].getAdditive().getApprovalStatus().toString()=="Unknown"){
                    color= getResources().getColor(R.color.colorUnknown);
                }else if (reponses[jj].getAdditive().getApprovalStatus().toString()=="Approved"){
                    getResources().getColor(R.color.colorAccepted);
                }else if (reponses[jj].getAdditive().getApprovalStatus().toString()=="Rejected"){
                    color= getResources().getColor(R.color.colorReject);
                }

                Date dateOriginal=new Date(reponses[jj].getOriginalTimestamp());
                SimpleDateFormat df2 = new SimpleDateFormat("dd/MM/yy");
                String originalTimeStr = df2.format(dateOriginal);

                Date dateLastCheck =new Date(reponses[jj].getTimestamp());
                String lastTimeStr = df2.format(dateLastCheck);

                mainData.add(new Sostanza(reponses[jj].getAdditive().getDisplayName(), reponses[jj].getApprovalStatus().toString(), originalTimeStr,lastTimeStr ,"","","","", color));
                elemAvail=true;
            }
        }
        adapter = new ListViewAdapter(this, mainData);
        list.setAdapter(adapter);

        ProgressBar progressBar = (ProgressBar) findViewById(R.id.LoadingSubstance);
        progressBar.setVisibility(View.GONE);
    }

    boolean elemAvail = false;
    @Override
    public boolean onQueryTextSubmit(String query) {
        ProgressBar progressBar = (ProgressBar) findViewById(R.id.LoadingSubstance);
        progressBar.setVisibility(View.VISIBLE);
        if(query=="")
            getResultCard("a");
        else
            getResultCard(query);
        elemAvail=true;
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if(elemAvail){
            getResultCard(newText);
            String text = newText;
            adapter.filter(text);
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
    /*
        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(R.menu.drawer, menu);
            return true;
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            int id = item.getItemId(); Context context=this;
            //noinspection SimplifiableIfStatement
            if (id == R.id.action_settings) {
                return true;
            }

            return super.onOptionsItemSelected(item);
        }
    */
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Context context = this;
        if (id == R.id.nav_analisi) {
            Intent intent = new Intent(context, MainActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_cronologia) {
            Intent intent = new Intent(context, HistoryActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_indice) {

        } else if (id == R.id.nav_manuale) {
            Intent intent = new Intent(context, GuideActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_aggiornamenti) {
            Intent intent = new Intent(context, UpdateActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_share) {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, R.string.share_message);
            sendIntent.setType("text/plain");
            startActivity(sendIntent);
        } else if (id == R.id.nav_send) {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_EMAIL,new String[] { getString(R.string.mail_recipient) }); // !
            sendIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.mail_subject));
            sendIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.email_body));
            sendIntent.setType("text/plain");
            startActivity(sendIntent);
        }else if (id == R.id.nav_credits) {
            Intent intent = new Intent(context,CreditsActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


}
