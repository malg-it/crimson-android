package it.malg.crimson;

import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class AdapterSmall extends RecyclerView.Adapter<AdapterSmall.SostanzaViewHolder> {

    private List<Sostanza> sostanzaList;

    public AdapterSmall(List<Sostanza> cards) {
        this.sostanzaList = cards;
    }

    @Override
    public SostanzaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_small, parent, false);


        return new SostanzaViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(SostanzaViewHolder holder, int position) {
        Sostanza ind = sostanzaList.get(position);
        holder.nomeText.setText(ind.getNome());
        holder.responsoText.setText(ind.getResponso());
        holder.dataControlloText.setText(ind.getDataControllo());
        holder.pallinoImage.setColorFilter(ind.getColore());
    }

    @Override
    public int getItemCount() {
        return sostanzaList.size();
    }

    public static class SostanzaViewHolder extends RecyclerView.ViewHolder {
        TextView nomeText;
        TextView responsoText;
        TextView dataControlloText;
        ImageView pallinoImage;

        public SostanzaViewHolder(View itemView) {
            super(itemView);
            nomeText = (TextView) itemView.findViewById(R.id.nome_sostanza);
            responsoText = (TextView) itemView.findViewById(R.id.responso);
            dataControlloText = (TextView) itemView.findViewById(R.id.ultimo_controllo);
            pallinoImage = (ImageView) itemView.findViewById(R.id.simbolo_responso);

            CardView card = (CardView) itemView.findViewById(R.id.card_view);
            card.setOnClickListener(openDetail);
        }

        private View.OnClickListener openDetail = new View.OnClickListener() {
            public void onClick(View v) {
                Intent pass = new Intent(v.getContext(), DetailActivity.class);
                pass.putExtra("NAME_SUBSTANCE", ""+nomeText.getText());
                v.getContext().startActivity(pass);


            }
        };
    }
}
