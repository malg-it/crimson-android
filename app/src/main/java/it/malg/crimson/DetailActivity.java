package it.malg.crimson;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewStub;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import it.malg.crimson.model.Database;
import it.malg.crimson.model.Request;
import it.malg.crimson.model.Response;


public class DetailActivity extends AppCompatActivity {

    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.noItemView);
        relativeLayout.setVisibility(View.GONE);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView_detailed_card);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        Bundle bundle = getIntent().getExtras();
        String[] nameSubstance = {bundle.getString("NAME_SUBSTANCE")};

        String tmp = nameSubstance[0].toString();
        String substance = tmp.substring(0, tmp.indexOf(" ("));
        nameSubstance[0]=substance.replaceFirst("^\\s*", "");

        List<Sostanza> mainData = new ArrayList<>();

        Request request = new Request(nameSubstance);
        if (Database.getInstance().getAdditiveBySubquery(nameSubstance[0]).getCount() > 0) {
            Response reponses[] = request.getReponses();
            int color= getResources().getColor(R.color.colorAccepted);
            if (reponses[0].getAdditive().getApprovalStatus().toString()=="Unknown"){
                color= getResources().getColor(R.color.colorUnknown);
            }else if (reponses[0].getAdditive().getApprovalStatus().toString()=="Approved"){
                getResources().getColor(R.color.colorAccepted);
            }else if (reponses[0].getAdditive().getApprovalStatus().toString()=="Rejected"){
                color= getResources().getColor(R.color.colorReject);
            }

            Date dateOriginal=new Date(reponses[0].getOriginalTimestamp());
            SimpleDateFormat df2 = new SimpleDateFormat("dd/MM/yy");
            String originalTimeStr = df2.format(dateOriginal);

            Date dateLastCheck =new Date(reponses[0].getTimestamp());
            String lastTimeStr = df2.format(dateLastCheck);

            Date dateAppCheck =new Date(reponses[0].getAdditive().getApprovalTimestamp());
            String appTimeStr = df2.format(dateAppCheck);

            String url = reponses[0].getAdditive().getDocument();

            mainData.add(new Sostanza(reponses[0].getAdditive().getDisplayName(), reponses[0].getApprovalStatus().toString(), originalTimeStr,lastTimeStr , "", "", appTimeStr,url, color));
        }else{
            relativeLayout = (RelativeLayout) findViewById(R.id.noItemView);
            relativeLayout.setVisibility(View.VISIBLE);
        }

        RecyclerView.Adapter adapter = new AdapterDetail(mainData);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
/*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.drawer, menu);
        //      MenuItem item = menu.findItem(R.id.nav_share);
        //     mShareActionProvider = (ShareActionProvider) item.getActionProvider();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Context context = this;
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
   }
   */
}
