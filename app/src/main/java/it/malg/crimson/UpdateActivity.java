package it.malg.crimson;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import it.malg.crimson.model.Dataset;


public class UpdateActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public boolean statoConnessione(){
        Context context = getApplicationContext();
        ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

        if(isConnected){
            CharSequence text = "Connesione effettuata con "+activeNetwork.getTypeName();
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        }
        return isConnected;
    }

    boolean datasetDownloaded = Dataset.getInstance().isReady();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);
        Button button = (Button) findViewById(R.id.download_btn);
        TextView textView = (TextView) findViewById(R.id.message_update_lab);
        TextView updateResult = (TextView) findViewById(R.id.update_result_lab);
        //final ProgressBar downloadBar = (ProgressBar) findViewById(R.id.download_bar);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        if(datasetDownloaded) {
            navigationView.getMenu().clear();
            navigationView.inflateMenu(R.menu.activity_drawer);
        }else{
            navigationView.getMenu().clear();
            navigationView.inflateMenu(R.menu.activity_drawer_block);
        }

        //boolean stato=statoConnessione();

        //updateResult.setVisibility(View.GONE);

        if(datasetDownloaded) {
            textView.setText("The dataset is up-to-date.");
            button.setEnabled(false);
        }else{
            textView.setText("A new dataset is available!");
            button.setEnabled(true);
            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    new UpdateAsyncTask().execute();
                    //downloadBar.setVisibility(View.VISIBLE);

                }
            });
        }
    }

    class UpdateAsyncTask extends AsyncTask<Void,Void,Boolean> {

        ProgressBar downloadBar = (ProgressBar) findViewById(R.id.download_bar);
        TextView updateResult = (TextView) findViewById(R.id.update_result_lab);
        Button button = (Button) findViewById(R.id.download_btn);

        @Override
        protected void onPreExecute() {
            button.setEnabled(false);
            updateResult.setVisibility(View.VISIBLE);
            downloadBar.setVisibility(View.VISIBLE);
            updateResult.setText(" Download is starting ");
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                if (Dataset.getInstance().pull()) {
                    return true; // Successo
                }else{
                    throw new Exception("Operation failed.");
                }
            } catch (Exception ex) {
                return false;// Fallimento
            }

        }

        @Override
        protected void onPostExecute(Boolean backgroundResult) {
            downloadBar.setVisibility(View.GONE);
            if(backgroundResult){
                updateResult.setText(R.string.update_good);
                button.setEnabled(true);
                //restart app
                Intent intent = new Intent(UpdateActivity.this,SplashScreenActivity.class);
                startActivity(intent);
            }else{
                updateResult.setText(R.string.update_fail);
                button.setText("ReTry");
                button.setEnabled(true);
            }
            updateResult.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onBackPressed() {
        if(datasetDownloaded) {
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                super.onBackPressed();
            }
        }
    }
    /*
        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(R.menu.drawer, menu);
            return true;
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            int id = item.getItemId();
            if(datasetDownloaded) {
                if (id == R.id.action_settings) {
                    return true;
                }
            }
            return super.onOptionsItemSelected(item);
        }
    */
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        if(datasetDownloaded) {
            int id = item.getItemId();
            Context context = this;
            if (id == R.id.nav_analisi) {
                Intent intent = new Intent(context, MainActivity.class);
                startActivity(intent);
            } else if (id == R.id.nav_cronologia) {
                Intent intent = new Intent(context, HistoryActivity.class);
                startActivity(intent);
            } else if (id == R.id.nav_indice) {
                Intent intent = new Intent(context, IndexActivity.class);
                startActivity(intent);
            } else if (id == R.id.nav_manuale) {
                Intent intent = new Intent(context, GuideActivity.class);
                startActivity(intent);
            } else if (id == R.id.nav_aggiornamenti) {

            } else if (id == R.id.nav_share) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, R.string.share_message);
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            } else if (id == R.id.nav_send) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_EMAIL,new String[] { getString(R.string.mail_recipient) }); // !
                sendIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.mail_subject));
                sendIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.email_body));
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }else if (id == R.id.nav_credits) {
                Intent intent = new Intent(context,CreditsActivity.class);
                startActivity(intent);
            }
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }
}
