# _Crimson_ app project
Ingegneria del Software, A.A. 2016-17, gruppo MALG.

**[Download APK](https://bitbucket.org/malg-it/crimson-android/downloads/it.malg.crimson.apk)**, [Online help](https://crimson.readthedocs.io/en/latest/)

> You're permitted to install, use, distribute this app as per the [GPL v3](LICENSE.md) license terms. 
